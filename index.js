// console.log("Kessoku Band");

// Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/")
  .then((response) => response.json())
  .then((json) => console.log(json));

// sing the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.


  fetch("https://jsonplaceholder.typicode.com/todos/")
  .then(response => response.json())
  .then((json) => json.map(data => data.title))
  .then((datas) => {

  	let titleData = datas;
  	console.log(titleData);
  })



 // Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

  fetch("https://jsonplaceholder.typicode.com/todos/2")
  .then((response) => response.json())
  .then((json) => console.log(json));


// Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

fetch("https://jsonplaceholder.typicode.com/todos/2")
.then((response) => response.json())
.then((retData) => {
	console.log(`The item "${retData.title}" on the list has a status of ${retData.completed}`);
})

//Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/",{

	method: "POST",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({

		title : "Create to do list item",
		userId: 1
	})
})
.then((response) => response.json())
.then((data) => console.log(data));

//Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
/*
Update a to do list item by changing the data structure to contain the following properties:
- Title
- Description
- Status
- Date Completed
- User ID
*/

fetch("https://jsonplaceholder.typicode.com/todos/2",{

	method:"PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({

		title: "Updated to do list item",
		description: "To update the my to do list with a different data structure",
		status: "Pending",
		dateCompleted: "Pending",
		userId: 1
	})
})
.then((response) => response.json())
.then((update) => console.log(update));

// Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API
// Update a to do list item by changing the status to complete and add a date when the status was changed.

fetch("https://jsonplaceholder.typicode.com/todos/2",{

	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({

		status: "Complete",
		dateCompleted: "02/30/2022"
	})
})
.then((response) => response.json())
.then((update1) => console.log(update1));

//Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/3",{

	method: "DELETE"
	
})